<?php

namespace SampleArticle\Subscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Psr\Container\ContainerInterface;

class SampleArticleRequestSubscriber implements EventSubscriberInterface
{


    public function __construct(ContainerInterface $container, EntityRepositoryInterface $productRepository, EntityRepositoryInterface $product_translationRepository) {
        $this->container = $container;
        $this->productRepository = $productRepository;
        $this->product_translationRepository = $product_translationRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => 'handleRequest'
        ];
    }


    public function handleRequest($request)
    {   
        // auf controller prüfen
        $requestRoute = $request->getRequest()->attributes->get('_route');

        if($requestRoute == "frontend.detail.page" ){
            
            $url = $request->getRequest()->server->get('REDIRECT_URL');


            // productnumber anders bekommen
            $productnumberURL = substr($url, strrpos($url, '/') + 1);
            $storefrontURL = $request->getRequest()->attributes->get('sw-storefront-url');

            $mainArticleData = $this->searchForMainArticle($productnumberURL);

            if($mainArticleData != null){

                $redirectURL = $this->container->get('router')->generate('frontend.detail.page', array('productId' =>  $mainArticleData));

                $request->setResponse(new RedirectResponse($redirectURL));  
            }
      }
    }

    public function searchForMainArticle($productnumber){
        $results = array();

        $mainProductsTranslation = $this->product_translationRepository->search(
            (new Criteria())->addFilter(new EqualsFilter('customFields.custom_sample_article_item_number', $productnumber)),
            \Shopware\Core\Framework\Context::createDefaultContext()
        );

        if($mainProductsTranslation->getTotal() > 0){

            // get first product id of main article in translation table
            $mainProductTranslation = $mainProductsTranslation->getEntities()->getElements();
            $first_product = array_key_first($mainProductTranslation);
            $first_product_id = $mainProductTranslation[$first_product]->getProductId();

            return $first_product_id;

        } else {
            return $results;
        }
    }

}


