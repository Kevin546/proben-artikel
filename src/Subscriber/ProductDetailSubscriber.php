<?php

namespace SampleArticle\Subscriber;

use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;

class ProductDetailSubscriber implements EventSubscriberInterface
{


    public function __construct(EntityRepositoryInterface $productRepository) {
        $this->productRepository = $productRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            ProductPageLoadedEvent::class => 'onProductPageLoaded'
        ];
    }

    public function onProductPageLoaded(ProductPageLoadedEvent $event)
    {
        $itemNumber = null;
        dump($event->getPage()->getProduct()->getcustomFields());
        $productCustomFields = $event->getPage()->getProduct()->getcustomFields();

        foreach($productCustomFields as $key=>$value){

            if($key == "custom_sample_article_item_number"){
                $itemNumber = $value;
            }
        }

        if($itemNumber){

            $sampleProduct = $this->productRepository->search(
                (new Criteria())->addFilter(new EqualsFilter('product.productNumber', $itemNumber)),
                \Shopware\Core\Framework\Context::createDefaultContext()
            );

            $event->getPage()->getProduct()->addExtension('sampleProduct', $sampleProduct);
        }

    }

}


